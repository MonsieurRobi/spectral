---
layout: page
title: "Demain promis"
kind: Chanson
pitch: "Y a t-il des dépressifs dans la salle ce soir ?"
duration: "4"
---
### Transition

Sortir un medoc, l'avaler avec un verre d'eau  
Tiens, ça me fait rappeleer qqchose  
Est-ce que vous êtes chaud pour une chansons sur la dépression ?

### Paroles

Demain je me lève tôt promis  
J'arrête de jouer les momies  
Demain jour de marché  
J'irai chez l'maraîcher  
Je mang'rai c'est promis

Demain salade et champignons
Haricots verts, pêches et brugnons  
Demain j'vid'la poubelle  
Et je fais la vaisselle  
Oui demain c'est promis

Mais aujourd'hui  
J'ai mal au coeur et j'ai pas faim  
J'ai le moral dans les chaussettes  
Et pas le courage d'être honnête  
Ni envie de quitter ma couette  
Je vais dormir jusqu'à demain  
Demain demain demain

Demain je m'ras' de près promis  
Je relook mon anatomie  
Je m'remuscle le dos  
Et je fais des abdos  
Demain plus d'compromis

Je force pas sur le Riesling  
Je laisse la voiture au parking  
J'irai même de bonne heure  
Faire un tour chez l'coiffeur  
Oui demain c'est promis

Mais aujourd'hui
Ca m'semble un travail de romain  
J'arriv'pas à m'brosser les dents  
Et contempler ma gueule dans  
Le miroir franch'ment c'est chiendent  
J'préfère me regarder demain  
Demain demain demain

Demain j'trie mes papiers promis  
Si je retrouve où j'les ai mis  
Demain l'aspirateur  
Ne me fera plus peur  
Demain oui les amis

Je sortirai d'mon aquarium  
Je répondrai au téléphone  
Je paierai mes factures  
Cette fois je le jure  
Oui demain c'est promis

Mais aujourd'hui  
Je veux pas passer d'examens  
J'ai pas vraiment envie d'parler  
J'préfère allumer la télé  
Me cloîtrer derrière mes volets  
Je prendrai ma vie à deux mains  
Demain demain demain
