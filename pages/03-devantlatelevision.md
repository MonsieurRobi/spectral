---
layout: page
title: "Devant la télévision"
kind: Chanson
pitch: "Chanson critique sur les media"
duration: "4"
---
### Paroles

Je parle très peu aux gens  
Dans mon p'tit pavillon de Nogent  
Depuis que je suis abonné au cable  
Je suis du genre peu affable  

Je reçois 150 chaînes  
Drogué aux séries américaines  
Les "what the fuck", "he's got a gun",
M'ont r'filé un p'tit coup de jeune

Passer sa vie devant la télévision  
Devrait m'emplir de consternation  
Seulement voilà, j'ai chopé une maladie  
Qui me cloue sur Canal Jimmy

Rentré du bureau à 8h  
Je passe une heure avec Jack Bauer  
Je rêves d'une mission à Caracas  
D'un nom emprunté, d'un Alis  

Passer sa vie devant la télévision
N'est pas la meilleure des solutions  
Mais j'ai chopé une sorte de choléra  
Qui m'force à r'rgarder Téva

Je n'suis qu'un pauvre télé)addict  
Parfois même j'me shoote à Derrick  
Résister à Sydney Bristow  
"Can't miss the miss of the show"

J'imagine déjà mon enterrement  
D'avoir top mater mort d'épuisement  
Dans mon cercueil, ma télé, ma p'tite soeur  
Sans oublier mon frère mon décodeur

Passer sa vie devant la télévision  
Maintenant qu'j'suis mort, j'le prends en dérision  
Six feet under, j'ai vu une prise de terre  
J'ai branché la télé, invité les vers de terre...

### Transition
Terminer en faisant un appel via l'app à l'écriture d'une chanson traitant vraiment de critique media

