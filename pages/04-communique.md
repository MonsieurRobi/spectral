---
layout: page
title: Communiqué
kind: Sketch
pitch: "Maintenant que vous connaissez mon univers, ecrivons un communiqué de presse"
duration: "3"
---
### Redaction du communiqué

Vous savez quoi ? Maintenant que vous connaissez mon univers, il est temps d'écrire un communiqué de presse. Et puis je préfère être prudent, et l'écrire en début de spectacle, on sait jamais !
