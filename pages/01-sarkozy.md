---
layout: page
title: Sarkozy
kind: Chanson
pitch: "Je donn'rai ma vie à Nicolas Sarkozy"
duration: "3"
---
### Sarkozy

J'donnerai ma vie à Nicolas Sarkozy 
J'donnerai ma vie à Rachida Dati  
Un grand merci à Michelle Alliot Marie  
Mon saint patron, c'est François Fillon  

Tous les matins, j'écoute Europe 1  
Tous les matins, je prends mon train  
Tous les matins, je vais au turbin  
Vivement ce soir, que j'regarde TF1  

J'aime bien Dominique de Villepin  
J'aime bien Valéry Giscard d'Estaing  
Jaime bien le jeune François Baroin  
J'aime encore mieux Monsieur Hortefeux  

Près de chez moi, j'suis en sécurité  
Des caméras, y'en a tout plein mon quartier  
Près de chez moi, aucun étranger  
Et oui et oui, on les a expulsés  

Un grand merci à Nicolas Sarkozy  
Un grand merci à Rachida Dati  
Un grand merci à Michèle Alliot-Marie  
Sans oublier Philippe de Villiers  
