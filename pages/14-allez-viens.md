A demain, pour tes mains
Tes mains prenant mes seins
A deux mains, c'est certain
Car il n'y en a pas qu'un
Sois câlin mon coquin, je le mérite bien
Et puis, toi à la fin, ne sois pas incertain

Allez, viens, allez viens

Ce matin, mon coquin, j'ai senti tes deux mains
Sur le bas de mes reins faire des p'tits va-et-vient
Sais-tu où sont mes seins, pourtant on les voit bien
Mais depuis ce matin, j'en doute un brin

Je me demande bien ce que sera demain
Si on a le béguin, ce peut être divin
De frémir sans enfrain, sans y être contraint
Tu sors ton bel engin, et je suis sur ma fin

Te voilà, c'est soudain, tu chahutes mes reins
Je me cramponne enfin à ton large bassin
Tu m'emmènes au lointain sur tes airs libertins
Tu seras le gardien de mon jardin

Allez viens ad libitum

